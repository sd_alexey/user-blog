<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/blog/category/{slug?}', 'BlogController@category')->name('category');
Route::get('/blog/article/{slug?}', 'BlogController@article')->name('article');

Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>['auth']], function(){
    Route::get('/', 'DashboardController@dashboard')->name('admin.index');
    Route::resource('/category', 'CategoryController', ['as'=>'admin']);
    Route::resource('/article', 'ArticleController', ['as'=>'admin']);
    Route::resource('/tag', 'TagController', ['as'=>'admin']);
});

Route::get('/', function () {
    return view('layouts.main');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/search/{searchKey}', 'HomeController@search');


Route::get('/login/{social}','Auth\LoginController@socialLogin')
    ->where('social','twitter|facebook|google');
Route::get('/login/{social}/callback','Auth\LoginController@handleProviderCallback')
    ->where('social','twitter|facebook|google');

Route::get('/user/activation/{token}', 'Auth\RegisterController@userActivation');

/*Route::get('/search', function (Request $request) {
    $result = App\Tag::search($request->search)->get();
    return view('layouts.result', compact('result'));
});*/