<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use App\Tag;

class BlogController extends Controller
{
    public function category($slug)
    {
        $category= Category::where('slug', $slug)->first();

        return view('blog.category', [
            'category' => $category,
            'articles' => $category->articles()->where('published', 1)->paginate(12),
            'users'   => User::pluck('name', 'id'),
        ]);
    }
    public function article($slug)
    {
        return view('blog.article', [
            'article' => Article::where('slug', $slug)->first(),
            'users'   => User::pluck('name', 'id'),
        ]);
    }
    public function app(){
        return view('layouts.main', [
            'tags' => Tag::lastTags(),
            'articles' => Article::lastArticles(10),
            'count_tags' => Tag::count(),
            'count_articles' => Article::count()
        ]);
    }
}
