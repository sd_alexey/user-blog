<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //Dashboard
    public function dashboard(){
        return view('admin.dashboard', [
            'tags' => Tag::lastTags(),
            'articles' => Article::lastArticles(5),
            'count_tags' => Tag::count(),
            'count_articles' => Article::count()
        ]);
    }
}
