<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class BlogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->topMenu();
        $this->app();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app();
    }

    public function topMenu()
    {
        View::composer('layouts.header', function ($view){
            $view->with('categories', \App\Category::where('parent_id', 0)->where('published', 1)->get());
        });
    }

    public function app()
    {
        View::composer('layouts.main', function ($view){
            $view->with('articles', \App\Article::get());
        });
        View::composer('layouts.blog.main_post', function ($view){
            $view->with('users', \App\User::pluck('name', 'id'));
        });
    }
}
