<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Tag extends Model
{

    use Searchable;

    public function searchableAs()
    {
        return 'tags';
    }

    protected $fillable = ['name'];

    public function articles()
    {
        return $this->belongsToMany('App\Article')->withTimestamps();
    }

    public function scopeLastTags($query)
    {
        return $query->orderBy('created_at', 'desc')->get();
    }
}
