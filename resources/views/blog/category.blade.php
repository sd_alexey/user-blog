@extends('layouts.app')


@section('title', $category->title . " - User-Blog")

@section('content')
    <div class="single">
        <div id="wrapper">
        @forelse($articles as $article)
            <article class="post">
                <header>
                    <div class="title">
                        <h2><a href="{{route('article', $article->slug, $article->created_by)}}">{{$article->title}}</a></h2>
                        <p>{{$article->meta_description}}</p>
                    </div>
                    <div class="meta">
                        <time class="published" datetime="2015-11-01">{{$article->created_at}}</time>
                        <a href="#" class="author"><span class="name">{{$users[$article->created_by]}}</span><img src="images/avatar.jpg" alt="" /></a>
                    </div>
                </header>
                <a href="{{route('article', $article->slug)}}" class="image featured"><img src="images/pic01.jpg" alt="" /></a>
                {!! $article->description_short!!}
                <footer>
                    <ul class="actions">
                        <li><a href="{{route('article', $article->slug)}}" class="button large">Continue Reading</a></li>
                    </ul>
                    <ul class="stats">
                        <li><a href="#">{{  $article->tags->pluck('name')->implode(', ') }}</a></li>
                        <li><a href="#" class="icon fa-heart">28</a></li>
                        <li><a href="#" class="icon fa-comment">128</a></li>
                    </ul>
                </footer>
            </article>

        @empty
            <h2 class="text-center">Пустой</h2>
        @endforelse
        {{$articles->links()}}
        </div>
    </div>

@endsection