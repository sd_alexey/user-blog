@extends('layouts.app')

@section('title', $article->meta_title)
@section('meta_keyword', $article->meta_keyword)
@section('description', $article->meta_description)

@section('content')

    <div class="single">
        <div id="wrapper">
            @include('layouts.header')


            <div id="main">
                <article class="post">
                    <header>
                        <div class="title">
                            <h2><a href="#">{{$article->title}}</a></h2>
                            <p>{{$article->meta_description}}</p>
                        </div>
                        <div class="meta">
                            <time class="published" datetime="2015-11-01">{{$article->created_at}}</time>
                            <a href="#" class="author">
                                <span class="name">{{$users[$article->created_by]}}</span>
                                <img src="images/avatar.jpg" alt="" />
                            </a>
                        </div>
                    </header>
                    <span class="image featured"><img src="{{$article->image}}" alt="" /></span>
                    @if($article->video)
                        <iframe width="560" height="315" src="{{$article->video}}?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    @endif
                        {!! $article->description!!}
                    <footer>
                        <ul class="stats">
                            <li><a href="#">{{  $article->tags->pluck('name')->implode(', ') }}</a></li>
                            <li><a href="#" class="icon fa-heart">28</a></li>
                            <li><a href="#" class="icon fa-comment">128</a></li>
                        </ul>
                    </footer>
                </article>
            </div>
        </div>
    </div>

@endsection