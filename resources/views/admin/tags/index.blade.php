@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('title') Список тегов @endslot
            @slot('parent') Главная @endslot
            @slot('active') Теги @endslot
        @endcomponent

        <hr>
        <a href="{{route('admin.tag.create')}}" class="btn btn-primary pull-right">
            <i class="fa fa-plus-square-o"></i> Создать тег
        </a>
        <table class="table table-striped">
            <thead>
            <th>Наименование</th>
            <th class="text-right">Действие</th>
            </thead>
            <tbody>
            @forelse($tags as $tag)
                <tr>
                    <td>{{$tag->name}}</td>
                    <td class="text-right">
                        <form action="{{route('admin.tag.destroy', $tag)}}"
                              onsubmit="if(confirm('Удалить?')){ return true }else{ return false }"
                              method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}

                            <a href="{{route('admin.tag.edit', $tag)}}" class="btn btn-default">
                                <i class="fa fa-edit"></i>
                            </a>

                            <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center"><h2>Данныйе отсуствуют</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="3">
                    <ul class="pagination pull-right">
                        {{$tags->links()}}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection