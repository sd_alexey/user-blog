@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">

        @component('admin.components.breadcrumb')
            @slot('title') Редактирование тега @endslot
            @slot('parent') Главная @endslot
            @slot('active') теги @endslot
        @endcomponent

        <hr />

        <form class="form-horizontal"
              action="{{route('admin.tag.update', $tag)}}" method="post">
            <input type="hidden" value="put" name="_method">
            {{ csrf_field() }}

            {{-- Form include --}}
            @include('admin.tags.partials.form')

        </form>
    </div>

@endsection