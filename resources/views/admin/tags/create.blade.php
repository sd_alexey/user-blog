@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">

        @component('admin.components.breadcrumb')
            @slot('title') Создание тега @endslot
            @slot('parent') Главная @endslot
            @slot('active') теги @endslot
        @endcomponent

        <hr />

        <form class="form-horizontal" action="{{route('admin.tag.store')}}" method="post">
            {!! csrf_field() !!}

            {{-- Form include --}}
            @include('admin.tags.partials.form')

        </form>
    </div>

@endsection