@foreach ($tags as $tag)

    <option value="{{$tag->id or ""}}"

            @isset($article->id)
            @foreach ($article->tags as $tag_article)
            @if ($tag->id == $tag_article->id)
            selected="selected"
            @endif
            @endforeach
            @endisset

    >
        {!! $delimiter or "" !!}{{$tag->name or ""}}
    </option>

@endforeach