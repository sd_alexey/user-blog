<header id="header">
    <h1><a href="{{ url('/') }}">User-log</a></h1>
    <nav class="links">
        <ul>
            @include('layouts.top_menu', ['categories' => $categories])
        </ul>
    </nav>
    <nav class="main">
        <ul>
            <li class="search">
                <a class="fa-search" href="#search">Search</a>
                <form id="search" method="get" action="/search">
                    <input type="text" name="search" placeholder="Search" />
                </form>
            </li>
            <li class="menu">
                @include('layouts.menu')
                <a class="fa-bars" href="#menu">Menu</a>
            </li>
        </ul>
    </nav>
</header>