@extends('layouts.app')

@section('content')
    <div id="wrapper">
        @include('layouts.header')


        <div id="main">
            @foreach($articles as $article)
                @include('layouts.blog.main_post', $article)
            @endforeach

            <ul class="actions pagination">
                <li><a href="" class="disabled button large previous">Previous Page</a></li>
                <li><a href="#" class="button large next">Next Page</a></li>
            </ul>


        </div>
        <section id="sidebar">
        @include('layouts.sidebar')
        </section>

    </div>
@endsection